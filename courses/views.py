from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, Http404, HttpResponseRedirect
from .models import Course
from django.template import loader, RequestContext

# courses/add
# courses/id
# courses/



def index(request):

    #get all courses
    try:
        courses = Course.objects.all()
    except Course.DoesNotExist:
        #if does not exit
        raise Http404('Could not retrieve any courses.')

    #render template with data
    return render(request, 'index.html',{'courses':courses})

def courseDetail(request, course_id):
    #get course with data
    try:
        course = Course.objects.get(pk=course_id)
    except Course.DoesNotExist:
        #if does not exit
         raise Http404('This course does not exist.')
    #render detail template with data
    return render(request, "course_detail.html", {'course':course})

#this goes to the add course page
def addCourseView(request):
    return render(request, "course_form.html")

#this goes to the update course view
def updateCourseView(request, course_id):
    #get course with data
    try:
        course = Course.objects.get(pk=course_id)
    except Course.DoesNotExist:
        #if does not exit
         raise Http404('This course does not exist.')
    #render detail template with data
    return render(request, "course_form.html", {'course':course})

#this goes to the update course
def updateCourse(request, course_id):
    course = Course.objects.get(pk=course_id)
    if 'courseTitle' in request.POST:
        course.Title = request.POST.get("courseTitle")
    if 'courseDescription' in request.POST:
        course.Description = request.POST.get("courseDescription")
    if 'courseInstructor' in request.POST:
        course.Instructor = request.POST.get("courseInstructor")
    if 'courseDuration' in request.POST:
        course.Duration = request.POST.get("courseDuration")
    if request.FILES:
         course.CourseArt = request.FILES["courseArt"]

    course.save()
    return HttpResponseRedirect("/courses/" + course_id)

#this is the actual creation of the course
def createCourse(request):
    course = Course()
    if 'courseTitle' in request.POST:
        course.Title = request.POST.get("courseTitle")
    if 'courseDescription' in request.POST:
        course.Description = request.POST.get("courseDescription")
    if 'courseInstructor' in request.POST:
        course.Instructor = request.POST.get("courseInstructor")
    if 'courseDuration' in request.POST:
        course.Duration = request.POST.get("courseDuration")
    if request.FILES:
         course.CourseArt = request.FILES["courseArt"]
    course.save()
    return HttpResponseRedirect("/courses/")

def deleteCourse(request, course_id):
    try:
        course = Course.objects.get(pk=course_id)
    except Course.DoesNotExist:
        #if does not exit
         raise Http404('This course does not exist.')
    course.delete()
    return HttpResponseRedirect("/courses/")