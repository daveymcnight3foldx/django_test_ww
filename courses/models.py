from django.db import models

# Create your models here.

class Course(models.Model):
    Title = models.CharField(max_length=50)
    Description = models.CharField(max_length=500)
    Instructor = models.CharField(max_length=50)
    Duration = models.IntegerField()
    CourseArt = models.FileField(upload_to='courses/')
