from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

# courses/add
# courses/id
# courses/
# courses/change/id
urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^(?P<course_id>[0-9]+)/$', views.courseDetail, name="courseDetail"),
    url(r'^add/', views.addCourseView, name="coursePost"),
    url(r'^(?P<course_id>[0-9]+)/change', views.updateCourseView, name="coursePost"),
    url(r'^(?P<course_id>[0-9]+)/update', views.updateCourse, name="updateCourse"),
    url(r'^create', views.createCourse, name="createCourse"),
    url(r'^(?P<course_id>[0-9]+)/delete', views.deleteCourse, name="deleteCourse"),

] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
